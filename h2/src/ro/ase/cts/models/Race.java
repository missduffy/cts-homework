package ro.ase.cts.models;
import java.util.ArrayList;
import java.util.List;

import ro.ase.cts.exceptions.NoScoresException;

public class Race {
	private List<Participant> participants;
	private String name;

	public Race(){
		this.participants = new ArrayList<Participant>();
	}

	public void addParticipant(Participant participant){
		this.participants.add(participant);
	}

	public void removeParticipants() {
		this.participants.clear();
	}

	public List<Participant> getParticipants(){
		return this.participants;
	}
	
	//add a new participant to race with his/her scores sorted in ascending order
	public void addParticipantWithOrderedScores(Participant participant) throws NoScoresException{
		participant.sortScores();
		addParticipant(participant);
	}
}
