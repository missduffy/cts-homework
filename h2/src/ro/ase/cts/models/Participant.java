package ro.ase.cts.models;

import ro.ase.cts.exceptions.InvalidEmailException;
import ro.ase.cts.exceptions.InvalidScoresException;
import ro.ase.cts.exceptions.NoScoresException;
import ro.ase.cts.exceptions.ParticipantAgeException;

public class Participant {
	private String name;
	private String email;
	private float[] scores;
	int age;

	public static final int MIN_AGE = 16;
	public static final int MAX_AGE = 90;

	public Participant(){

	}

	public Participant(String name, String email, float[] scores, int age) throws ParticipantAgeException, InvalidEmailException, InvalidScoresException{
		setName(name);
		setEmail(email);
		setAge(age);
		setScores(scores);
	}

	public float[] getScores(){
		return scores;
	}

	public void setScores(float[] scores) throws InvalidScoresException{
		if(scores == null) {
			throw new InvalidScoresException("The scores should not be empty");
		}
		this.scores = scores.clone();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws InvalidEmailException {
		if(email.contains("@") && email.contains(".ro") || email.contains(".com")) {
			this.email = email;
		}
		else {
			throw new InvalidEmailException("The email should be a valid one");
		}
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) throws ParticipantAgeException{
		if(age < Participant.MIN_AGE){
			throw new ParticipantAgeException("The age should be above 16");
		}
	}

	public void sortScores() throws NoScoresException{
		if(this.scores.length == 0) {
			throw new NoScoresException("The scores should contain at least one number");
		}
		for(int i=0; i<this.scores.length - 1; i++){
			for(int j=i+1; j<this.scores.length; j++) {
				if(scores[i] > scores[j]){
					float temp = scores[i];
					scores[i] = scores[j];
					scores[j] = temp;
				}
			}
		}
	}

}
