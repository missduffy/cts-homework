package ro.ase.cts.exceptions;

public class ParticipantAgeException extends Exception {

	public ParticipantAgeException(String message) {
		super(message);
	}
	
}
