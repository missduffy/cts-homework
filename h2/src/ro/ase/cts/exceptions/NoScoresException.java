package ro.ase.cts.exceptions;

public class NoScoresException extends Exception{
	
	public NoScoresException(String message) {
		super(message);
	}
}
