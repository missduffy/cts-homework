package ro.ase.cts.exceptions;

public class InvalidScoresException extends Exception{
	public InvalidScoresException(String message) {
		super(message);
	}
}
