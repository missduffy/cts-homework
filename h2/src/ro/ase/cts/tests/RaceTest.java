package ro.ase.cts.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ro.ase.cts.exceptions.InvalidEmailException;
import ro.ase.cts.exceptions.InvalidScoresException;
import ro.ase.cts.exceptions.NoScoresException;
import ro.ase.cts.exceptions.ParticipantAgeException;
import ro.ase.cts.models.Participant;
import ro.ase.cts.models.Race;

public class RaceTest {
	public Race race;

	@Before
	public void setUp() {
		race = new Race();
	}
	
	@After
	public void tearDown() {
		race.removeParticipants();
	}

	//Conformance test
	@Test 
	public void testConformanceEmail(){
		float[] scores = {3,2,1};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			fail("The email should be valid");
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
		
	} 
	
	//Ordering test
	@Test 
	public void testOrderScores(){
		float[] scores = {3, 2, 1};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			float[] actual = participant.getScores();
			float[] expected = {1, 2, 3};
			assertArrayEquals(expected, actual, 0.0001f);
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			fail("The test has failed");
		}
		
	} 

	//Range test + Boundary test
	@Test 
	public void testRangeAge(){
		float[] scores = {3,2,1};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 13);
			race.addParticipantWithOrderedScores(participant);
			fail("The age should be above 16");
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	//Reference test + Right values
	@Test
	public void testReferenceScores() {
		float[] scores = {6, 4.4f, 3, 9, 24, 42.4f};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			float[] actual = participant.getScores();
			float[] expected = {3, 4.4f, 6, 9, 24, 42.4f};
			scores[0] = 10;
			assertArrayEquals(expected, actual, 0.0001f);
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Existence test
	@Test
	public void testExistenceScores() {
		float[] scores = null;
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			fail("Error");
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Cardinality tests
	@Test
	public void testCardinalityZero() {
		float[] scores = new float[0];
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			fail("Error");
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testCardinalityOne() {
		float[] scores = {5.75f};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			float[] actual = participant.getScores();
			float[] expected = {5.75f};
			assertArrayEquals(expected, actual, 0.0001f);
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Time test + Performance test
	@Test
	public void testTime() {
		float[] scores = {6, 4.4f, 3, 9, 24, 42.4f};
		Participant participant;
		try {
			long start = System.currentTimeMillis();
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			long finish = System.currentTimeMillis();
			if((finish - start) >= 100) {
				fail("Takes too much time");
			}	
			else {
				assertTrue(true);
			}
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Test inverse relationships
	@Test
	public void testInverse() {
		float[] scores = {6, 4.4f, 3, 9, 24, 42.4f};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			List<Participant> participants = race.getParticipants();
			if(participants.contains(participant)) {
				assertTrue(true);
			}
			else {
				fail("Newly added object not found");
			}
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
	//Test cross-check
	@Test
	public void crossCheckOrder() {
		float[] scores = {6, 8, 16, 43, 3, 5};
		Participant participant;
		try {
			participant = new Participant("John Doe", "joedoe@gmail.com", scores, 20);
			race.addParticipantWithOrderedScores(participant);
			float[] actual = participant.getScores();
			float[] cross = {6, 8, 16, 43, 3, 5};
			Arrays.sort(cross);
			assertArrayEquals(cross, actual, 0.0001f);
		} catch (ParticipantAgeException | InvalidEmailException | InvalidScoresException | NoScoresException e) {
			System.out.println(e.getMessage());
		}
	}
	
}
