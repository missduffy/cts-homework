package ro.ase.cts.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ro.ase.cts.exceptions.InvalidDatesException;

public class DateUtils {
	
	public static String computeDuration(String start, String end) throws InvalidDatesException, ParseException{
		String duration = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		
		String timeStamp = sdf.format(Calendar.getInstance().getTime());
		if(start.equals(timeStamp)) {
			throw new InvalidDatesException("Dates shouldn't be the same!");
		}
		
		Date startDate = sdf.parse(start);
		Date endDate = sdf.parse(end);
		
		long difference = endDate.getTime() - startDate.getTime();
		
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;
		
		long elapsedDays = difference / daysInMilli;
		difference = difference % daysInMilli;

		long elapsedHours = difference / hoursInMilli;
		difference = difference % hoursInMilli;

		long elapsedMinutes = difference / minutesInMilli;
		difference = difference % minutesInMilli;

		long elapsedSeconds = difference / secondsInMilli;
		    
		duration = String.format("%d days, %d hours, %d minutes, %d seconds", 
			        elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
		
		return duration;
	}
}
