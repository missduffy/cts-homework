package ro.ase.cts.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Test;

import ro.ase.cts.exceptions.InvalidDatesException;
import ro.ase.cts.utils.DateUtils;

public class DateUtilsTests {
	
	@Test
	public void testSameDayDifference() {
		try {
			assertEquals("0 days, 2 hours, 0 minutes, 1 seconds", DateUtils.computeDuration("10.03.2019 12:08:12 ", "10.03.2019 14:08:13"));
		} catch (InvalidDatesException | ParseException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void testDifferentDaysDifference() {
		try {
			assertEquals("4 days, 5 hours, 33 minutes, 16 seconds", DateUtils.computeDuration("10.03.2019 18:24:40 ", "14.03.2019 23:57:56"));
		} catch (InvalidDatesException | ParseException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void testDifferentDatesDifference() {
		try {
			assertEquals("29 days, 7 hours, 0 minutes, 11 seconds", DateUtils.computeDuration("01.03.2019 06:00:01 ", "30.03.2019 13:00:12"));
		} catch (InvalidDatesException | ParseException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void testSwitchedParametersDifference() {
		try {
			assertEquals("-27 days, 0 hours, 0 minutes, 0 seconds", DateUtils.computeDuration("30.03.2019 06:00:00", "3.03.2019 06:00:00"));
		} catch (InvalidDatesException | ParseException e) {
			e.getMessage();
		}
	}
	
	@Test
	public void testDifferentMonthDifference() {
		try {
			assertEquals("35 days, 3 hours, 7 minutes, 28 seconds", DateUtils.computeDuration("23.05.2018 14:08:25 ", "27.06.2018 17:15:53"));
		} catch (InvalidDatesException | ParseException e) {
			fail("The method should not support this date format!");
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testWrongDifference() {
		try {
			assertEquals("35 days, 2 hours, 4 minutes, 28 seconds", DateUtils.computeDuration("23.05.2018 14:08:25 ", "27.06.2018 17:15:53"));
		} catch (InvalidDatesException | ParseException e) {
			fail("The method should not support this date format!");
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testWrongFormatDifference() {
		try {
			String result = DateUtils.computeDuration("10/03/2019", "14/03/2019");
		} catch (InvalidDatesException | ParseException e) {
			fail("The method should not support this date format!");
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testWrongFormat() {
		try {
			String result = DateUtils.computeDuration("10.03.2019", "14.03.2019");
		} catch (InvalidDatesException | ParseException e) {
			fail("The method should not support this date format!");
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testNowDifference() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			String timeStamp = sdf.format(Calendar.getInstance().getTime());
			assertEquals("0 days, 0 hours, 0 minutes, 0 seconds", DateUtils.computeDuration(timeStamp, timeStamp));
		} catch (ParseException e) {
			fail("The method should not support this date format!");;
		} catch (InvalidDatesException e) {
			fail("The start date and end date shouldn't be the current date!");
		}
	}
	
	@Test
	public void testInvalidFormatAndCurrentDateDifference() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String timeStamp = sdf.format(Calendar.getInstance().getTime());
			assertEquals("0 days, 0 hours, 0 minutes, 0 seconds", DateUtils.computeDuration(timeStamp, timeStamp));
		} catch (ParseException e) {
			fail("The method should not support this date format!");;
		} catch (InvalidDatesException e) {
			fail("The start date and end date shouldn't be the current date!");
		}
	}

}
