package ro.ase.cts.exceptions;

public class InvalidDatesException extends Exception{
	
	public InvalidDatesException(String message) {
		super(message);
	}
}
