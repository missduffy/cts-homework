Software Quality & Testing
Homework 3

The application that I'm currently developing is an Agile project management web application. It allows users to manage their projects and the tasks they have, as well as generate reports regarding the productivity and performances.

For the creational design patterns I would use:

 - Singleton
As there would also be a mobile application for this platform, a Singleton pattern could be used to enable the unique management of preferences on the Android platform - SharedPreferences.

 - Factory 
I would use the Factory pattern to implement the different types of positions that users have: managers, developers and so on.

 - Builder
The goal of this pattern is to separate complex construction layers in order to gain simpler objects. In Laravel, the framework that this application uses, an example of this is the AuthManager class, which needs to create some secure elements to reuse with selected auth storage drivers such as cookie, session, or custom elements. To achieve this, the AuthManager class needs to use storage functions such as callCustomCreator()and getDrivers() from the Manager class.

For the structural design patterns I would use:

 - Decorator
Because a project will have more users assigned to it, each having a specific role for that project, such as developer, project owner, project manager and so on, a Decorator could be useful. There would be classes that assign an attribute - the role of the user for that project - with names like DeveloperDecorator, ProjectManagerDecorator and so forth. Moreover, Decorators allow runtime changes, and as JavaScript is a dynamic language, the ability to extend an object at runtime is what the language is based on.

 - Fa�ade
For this design pattern, I would implement a class with a method that contains multiple calls to methods that change the settings of a user. For example, if a user wants to change the avatar, the username and email at the same time, this function would contain the calls to the necessary methods, each having its own behavior. A function that changes all of those settings with a single call.

 - Proxy
This design pattern would help with maintaining more platforms. For example, due to the fact that the application would have a mobile version, the Proxy pattern could manage the two platforms: the web proxy and the mobile one.

For the behavioral design patterns I would use:

 - Observer
Because the web application will make use of JavaScript, there will be a lot of events and listeners. Event handlers, functions that will be notified when a certain event fires, will be used. The event and event-handler paradigm in JavaScript is the manifestation of the Observer design pattern.

 - State
I would implement this design pattern in regards to the class that manages a task. As each task can have states like new, in progress, on feedback and finished, this would be useful.
 
 - Strategy
Because the REST API would interact with both the web application and the Android one, the Strategy pattern could be used to choose what to do, change the behavior.
